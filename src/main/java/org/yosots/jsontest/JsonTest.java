/**
 * @author Toshiyuki Yoshida
 * @version 0.0.2
 */


package org.yostos.jsontest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * JsonTest
 * を検証するためのサンプルコード.
 */
public class JsonTest {

    /**
     * メイン関数.
     */
    public static void main(String [] args) {
        String testJson1 =
            "{\"userid\":\"1234567890\", \"name\":\"Sazae Isono\"" 
            + ",\"attributes\":[\"female\",\"married\",\"spaz\"]}";

        System.out.println("\nオリジナル JSON----");
        System.out.println(testJson1);

        ObjectMapper mapper = new ObjectMapper()
                .enable(SerializationFeature.INDENT_OUTPUT);
        try {
            // JSONからJavaオブジェクトに変換
            User user1 = mapper.readValue(testJson1, User.class);

            // Javaオブジェクトのデータの確認
            System.out.println("\nJava Objectにマッピングした項目の確認----");
            System.out.println(user1.getUserid()); // 1
            System.out.println(user1.getName()); // taro
            System.out.println(user1.getAttributes()); // [基本, 応用]

            String json1 = mapper.writeValueAsString(user1);

            System.out.println("\n整形済み JSON----");
            System.out.println(json1);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
