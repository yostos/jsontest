/**
 * User class
 */

package org.yostos.jsontest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class User {

    private String userid;
    //name @JsonPropertyは、JSONのnameと紐付けする
    @JsonProperty("name")
    private String username;

    private List<String> attributes;
    
    public String getUserid() {
        return userid;
    }

    public String getName() {
        return username;
    }

    public List<String> getAttributes() {
        return attributes;
    }
}
