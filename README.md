# README

JSON整形の検証。


以下のようなベタ打ちのJSONを
```json
{"userid":"1234567890", "name":"Sazae Isono","attributes":["female","married","spaz"]}
```

以下のように整形した文字列にする。

```json
{
  "userid": "1234567890",
  "name": "Sazae Isono",
  "attributes": [
    "female",
    "married",
    "spaz"
  ]
}
```
